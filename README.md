TCO2019 Marathon Match Round 3 My Solution
==========================================


Challenge page   
https://www.topcoder.com/challenges/30103195  


Standings  
https://www.topcoder.com/challenges/30103195?tab=submissions  


Forum  
https://apps.topcoder.com/forums/?module=Category&categoryID=79325  


Review page (old platform?)  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30103195   


Review page (new platform)  
https://submission-review.topcoder.com/challenges/30103195   

