@echo off
@setlocal
@set MYCMD=java -cp classes Main
if "%~1"=="" goto defaultseed
java -jar tester.jar -exec "%MYCMD% -seed %*" -seed %* 2>&1
@GOTO finally
:defaultseed
java -jar tester.jar -exec "%MYCMD% -seed 1" -seed 1 2>&1
@GOTO finally
:finally
@endlocal