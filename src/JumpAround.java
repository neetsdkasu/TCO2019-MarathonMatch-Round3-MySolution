import java.io.*;
import java.util.*;

public class JumpAround {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    static Random rand = new Random(19831983L);

    static int[] DT = new int[]{ -1, 0, 1, 0, -1 };
    static char[] DTCH = new char[]{ 'U', 'R', 'D', 'L' };

    int N, NumPegs;
    char[] grid;
    int[][] dists;
    int[] pegs, targets;

    public String[] findSolution(int N, int NumPegs, char[] grid) {
        Timer timer = new Timer(9400L);

        this.N = N;
        this.NumPegs = NumPegs;
        this.grid = grid;

        pegs = new int[NumPegs];
        targets = new int[NumPegs];
        int pc = 0, tc = 0, wc = 0;
        for (int i = 0; i < grid.length; i++) {
            if (grid[i] == 'P') {
                pegs[pc] = i;
                pc++;
            } else if (grid[i] == 'X') {
                targets[tc] = i;
                tc++;
            } else if (grid[i] == '#') {
                wc++;
            }
        }

        System.err.println("N: " + N);
        System.err.println("NumPegs: " + NumPegs);
        System.err.println("Wall: " + wc);
        System.err.println("GridSize: " + (N * N));
        System.err.println(String.format("PegsRate: %.3f", (double)NumPegs / (double)(N * N)));
        System.err.println(String.format("WallRate: %.3f", (double)wc / (double)(N * N)));

        dists = new int[NumPegs][NumPegs];
        for (int i = 0; i < NumPegs; i++) {
            for (int j = 0; j < NumPegs; j++) {
                dists[i][j] = Math.abs(pegs[i] % N - targets[j] % N)
                    + Math.abs(pegs[i] / N - targets[j] / N);
            }
        }

        List<String> ret = new ArrayList<>();

        int[] pairs = new int[NumPegs];
        int[] order = new int[NumPegs];
        for (int i = 0; i < pairs.length; i++) {
            pairs[i] = i;
            order[i] = i;
        }

        int rem = solve(ret, pairs, order, false, N - 1);
        int sc = ret.size() + 2 * rem * N;

        LoopTimer loopTimer = timer.newLoopTimer();

        for (int cy = 0; cy < 100000000; cy++) {
            if (loopTimer.reachedLoopLimit()) {
                System.err.println("cycle: " + cy);
                break;
            }
            List<String> tmp = new ArrayList<>();
            shuffle(pairs);
            shuffle(order);
            int trem = solve(tmp, pairs, order, cy % 8 == 0, cy % 7 == 0 ? N : 3);
            int tsc = tmp.size() + 2 * trem * N;
            if (tsc < sc) {
                sc = tsc;
                ret = tmp;
            }
        }

        System.err.println("sc: " + sc);
        System.err.println("len: " + ret.size());
        System.err.println("avg: " + ((double)ret.size() / (double)NumPegs));
        System.err.println("rem: " + ((sc - ret.size()) / (2 * N)));

        return ret.toArray(new String[0]);
    }

    void shuffle(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            int j = rand.nextInt(i + 1);
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        }
    }

    void hcPairs(int[] pairs, int[] order, boolean withShuffle) {
        for (int cy = 0; cy < 100; cy++) {
            boolean change = false;
            for (int ii = 0; ii < pairs.length; ii++) {
                int i = order[ii];
                for (int jj = ii + 1; jj < pairs.length; jj++) {
                    int j = order[jj];
                    int dis1 = dists[i][pairs[i]] + dists[j][pairs[j]];
                    int dis2 = dists[i][pairs[j]] + dists[j][pairs[i]];
                    if (dis2 <= dis1) {
                        int p = pairs[i];
                        pairs[i] = pairs[j];
                        pairs[j] = p;
                        change = true;
                    }
                }
            }
            if (!change) {
                // System.err.println("breakHcPairs " + cy);
                break;
            }
            if (withShuffle) {
                shuffle(order);
            }
        }
    }

    int solve(List<String> ret, int[] pairs, int[] order, boolean withShuffle, int min0) {
        char[] grid = Arrays.copyOf(this.grid, this.grid.length);

        hcPairs(pairs, order, withShuffle);

        boolean[] moved = new boolean[NumPegs];
        int min = min0;
        for (int cy = 0; cy < 20; cy++) {
            boolean change = false;
            for (int ii = 0; ii < NumPegs; ii++) {
                int i = order[ii];
                if (moved[i]) {
                    continue;
                }
                moved[i] = dijkstra(ret, pegs[i], targets[pairs[i]], grid, min);
                change |= moved[i];
            }
            if (!change) {
                // System.err.println("breakMove " + cy);
                // break;
                min = Math.min(min + 1, N);
            }
            if (withShuffle) {
                shuffle(order);
            }
        }

        int count = 0;
        for (int i = 0; i < NumPegs; i++) {
            if (!moved[i]) {
                count++;
            }
        }

        return count;
    }

    boolean dijkstra(List<String> ret, int start, int goal, char[] grid, int min) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        int pattern = 8;
        int size = N * N;
        int[] visited = new int[size];
        visited[start] = pattern;
        pq.add(size + start);
        while (!pq.isEmpty()) {
            int e = pq.poll();
            int dis0 = e / size;
            int pos0 = e % size;
            if (pos0 == goal) {
                break;
            }
            if (dis0 == N || dis0 > min) {
                // System.err.println("NG " + start + "," + goal);
                return false;
            }
            if (visited[pos0] / pattern != dis0) {
                continue;
            }
            int row0 = pos0 / N;
            int col0 = pos0 % N;
            int dis = dis0 + 1;
            for (int di = 0; di < 4; di++) {
                int row = row0 + DT[di];
                int col = col0 + DT[di + 1];
                if (row < 0 || N <= row || col < 0 || N <= col) {
                    continue;
                }
                int pos = row * N + col;
                int g = grid[pos];
                if (g == '.' || g == 'X') {
                    if (visited[pos] == 0 || dis < visited[pos] / pattern) {
                        visited[pos] = dis * pattern + di;
                        pq.add(dis * size + pos);
                    }
                } else {
                    row += DT[di];
                    col += DT[di + 1];
                    if (row < 0 || N <= row || col < 0 || N <= col) {
                        continue;
                    }
                    pos = row * N + col;
                    g = grid[pos];
                    if (g == 'P' || g == '#') {
                        continue;
                    }
                    if (visited[pos0] % pattern >= 4) {
                        dis = dis0;
                    }
                    if (visited[pos] == 0 || dis < visited[pos] / pattern
                            || (dis == dis0 && dis == visited[pos] / pattern && visited[pos] % pattern < 4)) {
                        visited[pos] = dis * pattern + di + 4;
                        pq.add(dis * size + pos);
                    }
                }
            }
        }
        if (visited[goal] == 0) {
            return false;
        }
        // System.err.println("OK " + start + "," + goal + " : " + (visited[goal] / pattern) + " / " + N);
        List<String> cmds = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        int p = goal;
        int r = p / N, c = p % N;
        while (p != start) {
            int di = visited[p] % pattern;
            if (di >= 4) {
                di -= 4;
                sb.append(DTCH[di]);
                r -= DT[di] * 2;
                c -= DT[di + 1] * 2;
                p = r * N + c;
            } else {
                if (sb.length() > 0) {
                    cmds.add(String.format("%d %d J %s", r, c, sb.reverse().toString()));
                    sb.delete(0, sb.length());
                }
                r -= DT[di];
                c -= DT[di + 1];
                p = r * N + c;
                cmds.add(String.format("%d %d S %c", r, c, DTCH[di]));
            }
        }
        if (sb.length() > 0) {
            cmds.add(String.format("%d %d J %s", r, c, sb.reverse().toString()));
        }
        Collections.reverse(cmds);
        ret.addAll(cmds);
        grid[start] = '.';
        grid[goal] = 'P';
        return true;
    }
}


class Main {

    static int callFindSolution(BufferedReader in, JumpAround jumpAround) throws Exception {

        int N = Integer.parseInt(in.readLine());
        int NumPegs = Integer.parseInt(in.readLine());
        int _gridSize = N * N; //Integer.parseInt(in.readLine());
        char[] grid = new char[_gridSize];
        for (int _idx = 0; _idx < _gridSize; _idx++) {
            grid[_idx] = in.readLine().charAt(0);
        }

        Stopwatch sw = new Stopwatch();
        sw.start();

        String[] _result = jumpAround.findSolution(N, NumPegs, grid);

        sw.stop();
        System.err.println("time: " + sw.getTotalTime() + " ms");

        System.out.println(_result.length);
        for (String _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        JumpAround jumpAround = new JumpAround();

        callFindSolution(in, jumpAround);

    }

}


class Stopwatch {
    long startTime = 0L;
    long totalTime = 0L;
    public void reset() {
        startTime = 0;
        totalTime = 0;
    }
    public void start() {
        startTime = System.currentTimeMillis();
    }
    public long stop() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        totalTime += interval;
        return interval;
    }
    public long nowInterval() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        return interval;
    }
    public long nowTotal() {
        long stopTime = System.currentTimeMillis();
        long interval = stopTime - startTime;
        return totalTime + interval;
    }
    public long getTotalTime() {
        return totalTime;
    }
}

class LoopTimer {
    final long startTime;
    final long limitTime;
    long headTime;
    long lastTime;
    long interval;
    public LoopTimer(long loopLimit, long overallLimitTime) {
        startTime = System.currentTimeMillis();
        headTime = startTime;
        lastTime = startTime;
        limitTime = Math.min(startTime + loopLimit, overallLimitTime);
        interval = 0;
    }
    public boolean reachedLoopLimit() {
        lastTime = headTime;
        headTime = System.currentTimeMillis();
        interval = Math.max(interval, headTime - lastTime);
        return headTime + interval > limitTime;
    }
    public double progress() {
        return (double)(headTime - startTime) / (double)(limitTime - startTime);
    }
}

class Timer {
    final long startTime;
    final long limitTime;
    public Timer(long limit) {
        startTime = System.currentTimeMillis();
        limitTime = startTime + limit;
    }
    public LoopTimer newLoopTimer() {
        return new LoopTimer(limitTime - startTime, limitTime);
    }
    public LoopTimer newLoopTimer(long loopLimit) {
        return new LoopTimer(loopLimit, limitTime);
    }
    public LoopTimer newLoopTimer(long loopLimit, long breakLimit) {
        return new LoopTimer(loopLimit, Math.min(startTime + breakLimit, limitTime));
    }
    public double progress() {
        return (double)(System.currentTimeMillis() - startTime) / (double)(limitTime - startTime);
    }
}
